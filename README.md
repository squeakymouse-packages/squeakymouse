# squeakymouse
More packages for Pisi Linux
# How do I set this up
Open your package manager, then add a repository called squeakymouse.  
Then, copy the link below for the address of the repo:  
https://gitlab.com/squeakymouse-packages/squeakymouse/raw/master/pisi-index.xml.xz  
Alternatively, you can add the repository from the command line:  
sudo pisi add-repo squeakymouse https://gitlab.com/squeakymouse-packages/squeakymouse/raw/master/pisi-index.xml.xz  
Note: this repository works only with Pisi Linux 2.0    
# Caveats
To make ibus-anthy work, edit /etc/env.d/03scim. Remove all references to scim and xim and replace it with ibus as described below, with the XIM program being ibus-daemon.   
Add yourlocale.UTF-8 to your locale settings with:  
echo LANG="yourlocale.UTF-8" >> /etc/env.d/03locale (you need to do this as root with su; sudo doesn't suffice for some reason)     
If you're done with that, regenerate ld.so.cache with ldconfig and apply your settings with source /etc/profile     
If Japanese input still doesn't work, add   
export LANG="ja_JP.UTF-8"   
export QT_IM_MODULE="ibus"  
export GTK_IM_MODULE="ibus"     
export XMODIFIERS="@im=ibus"    
to your .bashrc as well as to your xprofile.    
    
Irrlicht does not tolerate the -Wl and -as-needed flags. Please do not use them in an Irrlicht project as using them would cause undefined reference errors.    
# Package sources
If you would like to build the packages yourself, check squeakymouse-descr 
